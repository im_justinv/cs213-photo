package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class representation of a User Object
 * @author Travis Thiel
 * @author Justin Valeroso
 */
public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	private String userName;
	private String password;
	private String accountType;
	public ArrayList<Album> albums;
	
	/**
	 * Constructor for a User instance
	 * @param userName is the User's username
	 * @param password is the User's password
	 * @param accountType is the User's account type (i.e. admin/user)
	 */
	public User (String userName, String password, String accountType) {
		this.userName = userName;
		this.password = password;
		this.accountType = accountType;
		this.albums = new ArrayList<Album>();
	}
	
	/**
	 * Gets the userName of this User
	 * @return userName User's username
	 */
	public String getUserName () {
		return userName;
	}
	
	/**
	 * Gets the password of this User
	 * @return password User's password
	 */
	public String getPassword () {
		return password;
	}
	
	/**
	 * Gets the account type of this User
	 * @return accountType User's account type
	 */
	public String getAccountType () {
		return accountType;
	}
	
	/**
	 * Gets the albums associated to the User
	 * @return albums an ArrayList of the User's associated Album
	 */
	public ArrayList<Album> getAlbums(){
		return this.albums;
	}
	
	/**
	 * Adds an Album to the User's albums
	 * @param a the Album object that will be added
	 */
	public void addAlbums(Album a) {
		System.out.println("adding: "+a.getName()+" to: "+albums.toString());
		this.albums.add(a);
	}
	
	/**
	 * Deletes an Album from the User's albums
	 * @param a the Album object that will be deleted
	 */
	public void deleteAlbum(Album a) {
		this.albums.remove(a);
	}
	
	/**
	 * Compares this instance of the User to the User passed through the parameter
	 * @param u the other User
	 * @return boolean value if the username and passwords match
	 */
	public boolean equals(User u){
		if (!(u instanceof User) || u == null) return false;
		return this.userName.equals(u.getUserName()) && this.password.equals(u.password);
	}
	
	/**
	 * Saves the serializable User to an ArrayList of stored Users
	 */
	public void saveUser () {
		try {
			// Deserialize storedUsers data
			FileInputStream fileIn = new FileInputStream("accounts.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
			in.close();
			fileIn.close();
					
			// Traverse storedUsers and save this User
			for (User u : storedUsers) {
				if (this.equals(u)) {
					storedUsers.set(storedUsers.indexOf(u), this);
				}
			}
					
			// Serialize updated storedUsers
			FileOutputStream fileOut = new FileOutputStream("accounts.dat");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(storedUsers);
			out.close();
			fileOut.close();
		}
		catch (ClassNotFoundException ex) {
			System.out.println("Class not found.");
		}
		catch (IOException ex) {
			System.out.println("Error reading file.");
		}	
	}
}
